CREATE TABLE usuario (
	id int NOT NULL AUTO_INCREMENT ,
	nome varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	senha varchar(255) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE funcao (
	id int NOT NULL AUTO_INCREMENT ,
	nome varchar(255) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE usuario_funcao (
	id_usuario int NOT NULL,
    id_funcao int NOT NULL,
	FOREIGN KEY (id_usuario) REFERENCES usuario(id) ON DELETE CASCADE,
	FOREIGN KEY (id_funcao) REFERENCES funcao(id) ON DELETE CASCADE
);
insert into usuario(nome, email, senha) values ('vitoria', 'vitoria@gmail.com', '$2a$10$t2VpwzJBGLaTNifbNyfUF.M8xzsnExOwBc4Yg./eUR6Rkqcrv/efK');
/*senha:1234*/