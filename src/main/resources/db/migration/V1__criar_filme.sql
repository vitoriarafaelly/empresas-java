CREATE TABLE filmes (
	id int NOT NULL AUTO_INCREMENT ,
	nome varchar(255) NOT NULL,
	genero varchar(255) NOT NULL,
	diretor varchar(255) NOT NULL,
	PRIMARY KEY (id)
);

insert into filmes(nome, genero, diretor) values ('Homem-Aranha: Sem Volta para Casa', 'ficção cintífica', 'Jon Watts');