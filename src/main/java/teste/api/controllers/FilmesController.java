package teste.api.controllers;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import teste.api.dtos.FilmesDto;
import teste.api.models.FilmesEntity;
import teste.api.services.FilmesService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class FilmesController {

    @Autowired
    FilmesService service;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/cadastrar")
    public ResponseEntity<FilmesEntity> cadastrarFilme(@RequestBody @Valid FilmesDto dto){
        FilmesEntity model = new FilmesEntity();
        BeanUtils.copyProperties(dto, model);
        service.cadastrar(model);
        return new ResponseEntity<>(model, HttpStatus.CREATED);

    }
    @GetMapping("/procurar-por-id")
    public Optional<FilmesEntity> procurarPorId(@RequestParam Long id){
        return service.findById(id);

    }
    @GetMapping("/procurar-por-nome")
    public Optional<FilmesEntity> procurarPorNome(@RequestParam String nome){
        return service.findByName(nome);

    }
    @GetMapping("/procurar-por-diretor")
    public Optional<FilmesEntity> procurarPorDiretor(@RequestParam String diretor){
        return service.findByDiretor(diretor);

    }
    @GetMapping("/procurar-por-genero")
    public Optional<FilmesEntity> procurarPorGenero(@RequestParam String genero){
        return service.findByGenero(genero);

    }

}
