package teste.api.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class FilmesDto {

    @NotBlank
    private String nome;
    @NotBlank
    private String dirtor;
    @NotBlank
    private String genero;
}
