package teste.api.forms;

import lombok.Data;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Data
public class LoginForm {
    private String email;
    private String senha;

    public UsernamePasswordAuthenticationToken dadosLogin(){
        return new UsernamePasswordAuthenticationToken(email, senha);
    }
}
