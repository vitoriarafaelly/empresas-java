package teste.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import teste.api.models.FilmesEntity;
import teste.api.repositories.FilmesRepository;

import java.util.Optional;

@Service
public class FilmesService {

    @Autowired
    FilmesRepository repository;

    public ResponseEntity cadastrar(@RequestParam FilmesEntity filme){
        try {
            repository.save(filme);
            return new ResponseEntity<>("Filme cadastrado com sucesso!", HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(
                    "Falha ao tentar cadastrar filme",
                    HttpStatus.BAD_REQUEST);
        }

    }

    public Optional<FilmesEntity> findById(Long id) {
        Optional<FilmesEntity> filme = repository.findById(id);
        return filme;
    }
    public Optional<FilmesEntity> findByName(String nome){
        Optional<FilmesEntity> filme = repository.findByName(nome);
        return filme;
    }
    public Optional<FilmesEntity> findByDiretor(String diretor){
        Optional<FilmesEntity> filme = repository.findByDiretor(diretor);
        return filme;
    }
    public Optional<FilmesEntity> findByGenero(String genero){
        Optional<FilmesEntity> filme = repository.findByGenero(genero);
        return filme;
    }

}
