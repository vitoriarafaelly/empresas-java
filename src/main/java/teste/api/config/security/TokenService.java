package teste.api.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import teste.api.models.UsuarioEntity;

import java.util.Date;

@Service
public class TokenService {
    @Value("${api.jwt.expiration}")
    private String tempoExp;

    @Value("${api.jwt.senha}")
    private String senha;
    public String gerarToken(Authentication authentication){
        UsuarioEntity usuarioEntity = (UsuarioEntity)authentication.getPrincipal();
        Date data = new Date();
        Date dataExp = new Date(data.getTime()+ Long.parseLong(tempoExp));
        return Jwts.builder()
                .setIssuer("API").setSubject(usuarioEntity.getId().toString()).setIssuedAt(data)
                .setExpiration(dataExp).signWith(SignatureAlgorithm.HS256, senha).compact();
    }

    public boolean isValido(String token) {
        try {
            Jwts.parser().setSigningKey(this.senha).parseClaimsJwt(token);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public Long getIdUsuario(String token) {
        Claims claims = Jwts.parser().setSigningKey(this.senha).parseClaimsJwt(token).getBody();
        return Long.parseLong(claims.getSubject());
    }
}
