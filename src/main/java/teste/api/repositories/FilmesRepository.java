package teste.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import teste.api.models.FilmesEntity;
import teste.api.models.UsuarioEntity;

import java.util.Optional;

public interface FilmesRepository extends JpaRepository<FilmesEntity, Long> {

    Optional<FilmesEntity> findById(Long id);
    Optional<FilmesEntity> findByName(String nome);
    Optional<FilmesEntity> findByDiretor(String diretor);
    Optional<FilmesEntity> findByGenero(String genero);
}
