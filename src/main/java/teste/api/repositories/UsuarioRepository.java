package teste.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import teste.api.models.UsuarioEntity;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {
    Optional<UsuarioEntity> findByEmail(String email);
}
