package teste.api.models;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Data
public class UsuarioEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String email;
    private String senha;
    @ManyToMany
    @JoinTable(name = "usuario_funcao", joinColumns = {@JoinColumn(name = "id_usuario", referencedColumnName = "Id")},
            inverseJoinColumns = {@JoinColumn(name = "id_funcao", referencedColumnName = "id")})
    private List<FuncaoEntity> funcao;

    public Long getId(){
        return this.id;
    }
    public List<FuncaoEntity> getFuncao(){
        return this.funcao;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.funcao;
    }

    @Override
    public String getPassword() {
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
