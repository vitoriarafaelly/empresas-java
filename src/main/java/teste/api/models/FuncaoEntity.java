package teste.api.models;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class FuncaoEntity implements GrantedAuthority {

    @Id
    private int id;
    private String nome;

    @Override
    public String getAuthority() {
        return nome;
    }
}
